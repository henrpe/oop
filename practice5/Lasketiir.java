class Lasketiir implements Lõbustus {

    @Override
    public void lõbusta(Külastaja külastaja) {
        int korda = annaJuhuslikNumber(0, 20);
        külastaja.lisaKirjeldus(String.format("tabasin lasketiirus %s sihtmärki", korda));
    }

    private int annaJuhuslikNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

}