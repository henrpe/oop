import java.util.ArrayList;
import java.util.List;

class Külastaja {

    private List<String> külastuseKirjeldused;
    private int vanus;
    private double kulud;

    Külastaja() {
        this.külastuseKirjeldused = new ArrayList<>();
    }

    Külastaja(int vanus) {
        this.vanus = vanus;
        this.külastuseKirjeldused = new ArrayList<>();
    }

    int getVanus() {
        return vanus;
    }

    void lisaKirjeldus(String kirjeldus) {
        külastuseKirjeldused.add(kirjeldus);
    }

    List<String> kõikKirjeldused() {
        return külastuseKirjeldused;
    }

    void lisaKulu(double kulu) {
        this.kulud += kulu;
    }

    double kuludeSumma() {
        return this.kulud;
    }

}
