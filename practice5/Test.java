import java.util.Arrays;

class Test {

    public static void main(String[] args) {
        Vaateratas vaateratas = new Vaateratas();
        TasulineLõbustus tasulineVaateratas = new TasulineLõbustus(2.25, vaateratas);

        Lasketiir lasketiir = new Lasketiir();
        TasulineLõbustus tasulineLasketiir = new TasulineLõbustus(1.5, lasketiir);
        VanuseKontrollija vanuseKontrollija = new VanuseKontrollija(10, tasulineLasketiir);

        Kloun kloun = new Kloun("Pepe");
        LõbustavKloun lõbustavKloun = new LõbustavKloun(kloun);

        Lõbustuspark lõbustuspark = new Lõbustuspark(Arrays.asList(tasulineVaateratas, vanuseKontrollija, lõbustavKloun));

        Külastaja külastaja9 = new Külastaja(9);
        Külastaja külastaja11 = new Külastaja(11);

        lõbustuspark.alustaSeiklust(külastaja9);
        lõbustuspark.alustaSeiklust(külastaja11);
    }

}
