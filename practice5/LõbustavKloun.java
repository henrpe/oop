class LõbustavKloun implements Lõbustus {

    private Kloun kloun;

    LõbustavKloun(Kloun kloun) {
        this.kloun = kloun;
    }

    @Override
    public void lõbusta(Külastaja külastaja) {
        kloun.esine(külastaja);
    }

}
