import java.util.List;

class Lõbustuspark {

    private List<Lõbustus> lõbustused;

    Lõbustuspark(List<Lõbustus> lõbustused) {
        this.lõbustused = lõbustused;
    }

    void alustaSeiklust(Külastaja külastaja) {
        System.out.println("alustan seiklust");
        for (Lõbustus lõbustus : lõbustused) {
            lõbustus.lõbusta(külastaja);
        }
        System.out.println("külastuste kirjeldused " + külastaja.kõikKirjeldused());
        System.out.println("kulude summa " + külastaja.kuludeSumma());
    }

}
