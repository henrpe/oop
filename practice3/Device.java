class Device {

  private static final double VAT_PERCENTAGE = 20;

  private int code;
  private String name;
  private double priceWithoutVat;

  Device() {
  }

  Device(int code, String name, double priceWithoutVat) {
    this.code = code;
    this.name = name;
    this.priceWithoutVat = priceWithoutVat;
  }

  int getCode() {
    return code;
  }

  void setCode(int code) {
    this.code = code;
  }

  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  double getPriceWithoutVat() {
    return priceWithoutVat;
  }

  void setPriceWithoutVat(double priceWithoutVat) {
    this.priceWithoutVat = priceWithoutVat;
  }

  double getPriceWithVat() {
    return priceWithoutVat * VAT_PERCENTAGE / 100 + priceWithoutVat;
  }

  @Override
  public String toString() {
    return "Device{" +
        "code=" + code +
        ", name='" + name + '\'' +
        ", priceWithoutVat=" + priceWithoutVat +
        ", priceWithVat=" + getPriceWithVat() +
        '}';
  }

}
