class DeviceApplication {

  public static void main(String[] args) {
    printDevice1Info();
    printDevice2Info();
    printDevice3Info();
  }

  private static void printDevice1Info() {
    Device device1 = new Device();
    device1.setCode(1);
    device1.setName("TV");
    device1.setPriceWithoutVat(1000);
    System.out.println(device1.toString());
  }

  private static void printDevice2Info() {
    Device device2 = new Device(2, "Radio", 500);
    System.out.println(device2.toString());
  }

  private static void printDevice3Info() {
    Device device3 = new Device(3, "Computer (mac)", 2000);
    System.out.println(device3.getCode());
    System.out.println(device3.getName());
    System.out.println(device3.getPriceWithoutVat());
    System.out.println(device3.getPriceWithVat());
  }

}
