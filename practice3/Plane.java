class Plane {

    private String name;
    private double speed;
    private int maxPassengerCount;

    Plane(String name, double speed, int maxPassengerCount) {
        this.name = name;
        this.speed = speed;
        this.maxPassengerCount = maxPassengerCount;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    double getSpeed() {
        return speed;
    }

    void setSpeed(double speed) {
        this.speed = speed;
    }

    int getMaxPassengerCount() {
        return maxPassengerCount;
    }

    void setMaxPassengerCount(int maxPassengerCount) {
        this.maxPassengerCount = maxPassengerCount;
    }

    @Override
    public String toString() {
        return "Plane{" +
            "name='" + name + '\'' +
            ", speed=" + speed +
            ", maxPassengerCount=" + maxPassengerCount +
            '}';
    }

}
