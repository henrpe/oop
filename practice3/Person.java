class Person {

  private String name;
  private double length;

  private Person(String name, double length) {
    this.name = name;
    this.length = length;
  }

  private int getSkiPoleLength() {
    return (int) Math.round(0.85 * length * 100);
  }

  public static void main(String[] args) {
    Person person = new Person("Joe", 1.8);
    System.out.println("Ski pole length is " + person.getSkiPoleLength() + " centimeters.");
  }

}