class Flight {

    private String destination;
    private int distance;
    private Plane plane;
    private int soldTickets;

    Flight(String destination, int distance, Plane plane, int soldTickets) {
        this.destination = destination;
        this.distance = distance;
        this.plane = plane;
        this.soldTickets = soldTickets;
    }

    String getDestination() {
        return destination;
    }

    int getDistance() {
        return distance;
    }

    Plane getPlane() {
        return plane;
    }

    int getSoldTickets() {
        return soldTickets;
    }

    int getDurationInMinutes() {
        return (int) (distance / plane.getSpeed() * 60);
    }

    int getVacantSeatsCount() {
        return plane.getMaxPassengerCount() - soldTickets;
    }

    @Override
    public String toString() {
        return "Flight{" +
            "destination='" + destination + '\'' +
            ", distance=" + distance +
            ", plane=" + plane +
            ", soldTickets=" + soldTickets +
            '}';
    }

}
