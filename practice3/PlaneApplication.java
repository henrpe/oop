class PlaneApplication {

    public static void main(String[] args) {
        Plane fokker = new Plane("Fokker F50", 530, 46);
        Plane bn = new Plane("BN-2", 170, 9);

        Flight flight1 = new Flight("Paris", 1907, fokker, 5);
        Flight flight2 = new Flight("London", 1861, fokker, 9);
        Flight flight3 = new Flight("Guadaloupe", 10133, bn, 2);

        System.out.println(flight1.toString());

        printFlightDurationAndSeatCount(flight1);
        printFlightDurationAndSeatCount(flight2);
        printFlightDurationAndSeatCount(flight3);
    }

    private static void printFlightDurationAndSeatCount(Flight flight) {
        System.out.println("Flight duration in minutes: " + flight.getDurationInMinutes());
        System.out.println("Flight vacant seat count: " + flight.getVacantSeatsCount());
    }

}
