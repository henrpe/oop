import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Linnuvaatlus {

  // This project has a directory in between the running directory and file(s)
  // If you don't have this directory in between then it should be removed from the following constant(s)
  // To find out the running directory: System.out.println(System.getProperty("user.dir"));
  private static final String INPUT_FILE = "KT2_2020/linnud.txt";
  private static final String OUTPUT_TXT_FILE = "KT2_2020/kaitsealused.txt";
  private static final String OUTPUT_DAT_FILE = "KT2_2020/linnud.dat";

  public static void main(String[] args) throws IOException {
    List<Lind> birds = loeLinnud(INPUT_FILE);
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
      while (true) {
        System.out.println("Do you want to add birds (L), view birds (V) or save/quit (S)?");
        String input = bufferedReader.readLine();
        if ("L".equals(input)) {
          addBirds(birds, bufferedReader);
        } else if ("V".equals(input)) {
          viewBirds(birds, bufferedReader);
        } else if ("S".equals(input)) {
          writeBirdsToDatFile(birds);
          writeUnderProtectionBirdsToTxtFile(birds);
          return;
        }
      }
    }
  }

  private static List<Lind> loeLinnud(String fileName) throws IOException {
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), UTF_8))) {
      return bufferedReader.lines().map(Lind::new).collect(Collectors.toList());
    }
  }

  private static Map<String, Integer> liigiKaupa(List<Lind> birds) {
    Map<String, Integer> birdSpeciesMap = new HashMap<>();
    for (Lind bird : birds) {
      birdSpeciesMap.put(bird.getLiik(), birdSpeciesMap.containsKey(bird.getLiik()) ? birdSpeciesMap.get(bird.getLiik()) + 1 : 1);
    }
    return birdSpeciesMap;
  }

  private static void addBirds(List<Lind> birds, BufferedReader bufferedReader) throws IOException {
    System.out.println("Bird info must be given in form of: 'species,colour,M/N,1/0':");
    try {
      birds.add(new Lind(bufferedReader.readLine()));
    } catch (ValeSuguErind valeSuguErind) {
      System.out.println(valeSuguErind.getMessage());
    }
  }

  private static void viewBirds(List<Lind> birds, BufferedReader bufferedReader) throws IOException {
    Map<String, Integer> birdSpeciesMap = liigiKaupa(birds);
    System.out.println("Bird species: " + birdSpeciesMap.keySet());
    while (true) {
      System.out.println("Which species' occurrence count would you like to see?");
      String input = bufferedReader.readLine();
      if (birdSpeciesMap.containsKey(input)) {
        System.out.println("Occurrence count for " + input + " is: " + birdSpeciesMap.get(input));
        return;
      }
    }
  }

  private static void writeBirdsToDatFile(List<Lind> birds) throws IOException {
    try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(OUTPUT_DAT_FILE))) {
      dataOutputStream.writeInt(birds.size());
      for (Lind bird : birds) {
        dataOutputStream.writeUTF(bird.getLiik());
        dataOutputStream.writeUTF(bird.getVärv());
        dataOutputStream.writeChar(bird.getSugu());
        dataOutputStream.writeBoolean(bird.isKaitseAll());
      }
    }
  }

  private static void writeUnderProtectionBirdsToTxtFile(List<Lind> birds) throws IOException {
    try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_TXT_FILE))) {
      for (Lind bird : birds) {
        if (bird.isKaitseAll()) {
          bufferedWriter.write(bird.getLiik() + " " + bird.getVärv() + " " + bird.getSugu() + "\n");
        }
      }
    }
  }

}
