public class Lind {

  private final String species;
  private final String colour;
  private final char gender;
  private final boolean isUnderProtection;

  public Lind(String birdData) {
    String[] birdDataParts = birdData.split(",");
    char gender = birdDataParts[2].charAt(0);
    if (gender != 'M' && gender != 'N') {
      throw new ValeSuguErind("Wrong gender. Gender must be 'M' or 'N' but was " + gender);
    }
    this.species = birdDataParts[0];
    this.colour = birdDataParts[1];
    this.gender = gender;
    this.isUnderProtection = "1".equals(birdDataParts[3]);
  }

  public String getLiik() {
    return species;
  }

  public String getVärv() {
    return colour;
  }

  public char getSugu() {
    return gender;
  }

  public boolean isKaitseAll() {
    return isUnderProtection;
  }

}
