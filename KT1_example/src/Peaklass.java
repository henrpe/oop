import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.shuffle;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

public class Peaklass {

  // This project has a directory in between the running directory and file(s)
  // If you don't have this directory in between then it should be removed from the following constant(s)
  // To find out the running directory: System.out.println(System.getProperty("user.dir"));
  private static final String INPUT_FILE = "KT1_example/lennud.txt";

  public static void main(String[] args) throws Exception {

    List<Lend> lennud = loeLennud(INPUT_FILE);

    lisaReisijaKolmeleSuvaliseleLennule(lennud, new TuristiklassiReisija("John Smith"));
    lisaReisijaKolmeleSuvaliseleLennule(lennud, new TuristiklassiReisija("Jane Doe"));
    lisaReisijaKolmeleSuvaliseleLennule(lennud, new EsimeseKlassiReisija(1));
    lisaReisijaKolmeleSuvaliseleLennule(lennud, new EsimeseKlassiReisija(2));

    sort(lennud);

    lennud.forEach(lend -> {
      System.out.println(lend);
      lend.väljastaReisijad();
    });
  }

  public static List<Lend> loeLennud(String failinimi) throws Exception {
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(failinimi), UTF_8))) {
      return bufferedReader.lines().map(rida -> rida.split(" ")).map(reaOsad -> reaOsad.length == 2 ?
        new Lend(reaOsad[0], parseDouble(reaOsad[1])) :
        new RahvusvahelineLend(reaOsad[0], parseDouble(reaOsad[2]), reaOsad[1])).collect(toList());
    }
  }

  private static void lisaReisijaKolmeleSuvaliseleLennule(List<Lend> lennud, Reisija reisija) {
    shuffle(lennud);
    lennud.get(0).transpordiReisija(reisija);
    lennud.get(1).transpordiReisija(reisija);
    lennud.get(2).transpordiReisija(reisija);
  }

}
