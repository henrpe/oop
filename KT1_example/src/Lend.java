import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.compare;

public class Lend implements Comparable<Lend> {

  private String sihtkoht;
  private double alghind;
  private List<Reisija> reisijad = new ArrayList<>();
  private double summa = 0;

  public Lend(String sihtkoht, double alghind) {
    this.sihtkoht = sihtkoht;
    this.alghind = alghind;
  }

  public String vormindaLennuinfo() {
    return "Lend: sihtkoht - " + sihtkoht;
  }

  public void transpordiReisija(Reisija reisija) {
    reisijad.add(reisija);
    summa += reisija.arvutaHind(alghind);
    reisija.salvestaLennuinfo(vormindaLennuinfo());
  }

  public void väljastaReisijad() {
    System.out.println(reisijad);
  }

  @Override
  public String toString() {
    return "Lend{" +
      "sihtkoht='" + sihtkoht + '\'' +
      ", summa=" + summa +
      '}';
  }

  @Override
  public int compareTo(Lend lend) {
    return this.summa != lend.summa ? compare(this.summa, lend.summa) : 0;
  }

}
