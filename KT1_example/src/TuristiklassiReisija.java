public class TuristiklassiReisija implements Reisija {

  private String nimi;
  private String lennuinfo;

  public TuristiklassiReisija(String nimi) {
    this.nimi = nimi;
  }

  @Override
  public double arvutaHind(double alghind) {
    return alghind;
  }

  @Override
  public void salvestaLennuinfo(String lennuinfo) {
    this.lennuinfo = lennuinfo;
  }

  @Override
  public String toString() {
    return "TuristiklassiReisija{" +
      "nimi='" + nimi + '\'' +
      ", lennuinfo='" + lennuinfo + '\'' +
      '}';
  }

}
