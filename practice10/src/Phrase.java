class Phrase {

  private final String phrase;

  Phrase(String phrase) {
    String reversedPhrase = new StringBuilder(phrase).reverse().toString();
    if (phrase.equals(reversedPhrase)) {
      throw new PalindromeException("we have a palindrome");
    }
    this.phrase = phrase;
  }

  @Override
  public String toString() {
    return "Phrase{" +
      "phrase='" + phrase + '\'' +
      '}';
  }

}
