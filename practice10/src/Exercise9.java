class Exercise9 {

    // https://www.geeksforgeeks.org/runnable-interface-in-java/
    public static void main(String[] args) {
        System.out.println("Main thread is: " + Thread.currentThread().getName());
        Thread thread = new Thread(new RunnableReader("practice10/testdata.txt"));
        thread.start();
        System.out.println("This line is printed before thread has finished printing lines");
    }

}
