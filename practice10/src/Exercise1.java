class Exercise1 {

  public static void main(String[] args) {
    try {
      Account account = new Account("John Smith", 1000);
      System.out.println(account.toString());

      account.buy(100);
      account.buy(2200);
      System.out.println(account.toString());

      account.addMoney(1000);
      System.out.println(account.toString());
    } catch (Exception exception) {
      System.out.println(exception);
    }
  }

}
