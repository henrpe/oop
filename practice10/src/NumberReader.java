import java.util.ArrayList;
import java.util.List;

class NumberReader {
    int[] method(String string) {
        String[] numberStrings = string.split(",");
        List<Integer> numberList = new ArrayList<>();
        for (String numberString : numberStrings) {
            try {
                if (numberString.isEmpty()) {
                    // instead of throwing EmptyNumberException and catching locally we could just System.out.println(..) the warning and use continue keyword
                    throw new EmptyNumberException();
                }
                numberList.add(Integer.parseInt(numberString));
            } catch (EmptyNumberException emptyNumberException) {
                System.out.println("Input must not be empty");
            } catch (NumberFormatException numberFormatException) {
                System.out.println("Input must be a number");
            }
        }
        return numberList.stream().mapToInt(i -> i).toArray();
    }
}
