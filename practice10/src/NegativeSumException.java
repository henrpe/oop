class NegativeSumException extends RuntimeException {

  NegativeSumException(String message) {
    super(message);
  }

}
