class PalindromeException extends RuntimeException {

  PalindromeException(String message) {
    super(message);
  }

}
