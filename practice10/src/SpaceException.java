class SpaceException extends RuntimeException {

    SpaceException(String message) {
        super(message);
    }

}
