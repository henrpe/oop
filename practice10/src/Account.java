class Account {

  private String name;
  private double money;

  Account(String name, double money) {
    this.setName(name);
    this.setMoney(money);
  }

  void setName(String name) {
    this.name = name;
  }

  void setMoney(double money) {
    if (money < 0) {
      throw new NegativeSumException("money can't be negative");
    }
    this.money = money;
  }

  void buy(double cost) {
    if (cost > this.money) {
      throw new InsufficientFundsException("not enough money in account");
    }
    this.money = this.money - cost;
  }

  void addMoney(double money) {
    this.setMoney(this.money + money);
  }

  @Override
  public String toString() {
    return "Account{" +
      "name='" + name + '\'' +
      ", money=" + money +
      '}';
  }

}
