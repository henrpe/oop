import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

class RunnableReader implements Runnable {

    private String fileName;

    RunnableReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run() {
        System.out.println("Runnable thread is: " + Thread.currentThread().getName());
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(fileName), Charset.defaultCharset())){
            bufferedReader.lines().forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException("File reading failed", e);
        }
    }

}
