import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Exercise5 {

    private static final List<String> BANNED_PUNCUTATIONS = Arrays.asList(" .", " !", " ?");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter text:");
            String input = scanner.nextLine();
            for (String bannedPunctuation : BANNED_PUNCUTATIONS) {
                if (input.contains(bannedPunctuation)) {
                    throw new SpaceException("Invalid puncutation found in input!");
                }
            }
        }
    }

}
