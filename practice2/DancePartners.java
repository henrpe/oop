import java.util.Arrays;

public class DancePartners {

  private static final int[] BOY_HEIGHTS = {180, 175, 200};
  private static final int[] GIRL_HEIGHTS = {165, 167, 172, 169, 162};

  public static void main(String[] args) {
    printOriginalHeights();
    sortHeights();
    printPairsAndLeftovers();
  }

  private static void printOriginalHeights() {
    System.out.println("Boys' heights are: " + Arrays.toString(BOY_HEIGHTS));
    System.out.println("Girls' heights are: " + Arrays.toString(GIRL_HEIGHTS));
  }

  private static void sortHeights() {
    Arrays.sort(BOY_HEIGHTS);
    Arrays.sort(GIRL_HEIGHTS);
  }

  private static void printPairsAndLeftovers() {
    int[] boyHeights = BOY_HEIGHTS;
    int[] girlHeights = GIRL_HEIGHTS;
    int[] boyHeightLeftovers = {};
    int[] girlHeightLeftovers = {};

    if (BOY_HEIGHTS.length > GIRL_HEIGHTS.length) {
      boyHeights = Arrays.copyOfRange(BOY_HEIGHTS, 0, GIRL_HEIGHTS.length);
      boyHeightLeftovers = Arrays.copyOfRange(BOY_HEIGHTS, GIRL_HEIGHTS.length, BOY_HEIGHTS.length);
    } else {
      girlHeights = Arrays.copyOfRange(GIRL_HEIGHTS, 0, BOY_HEIGHTS.length);
      girlHeightLeftovers = Arrays.copyOfRange(GIRL_HEIGHTS, BOY_HEIGHTS.length, GIRL_HEIGHTS.length);
    }
    printPairs(boyHeights, girlHeights);
    printWithoutPartner(boyHeightLeftovers, "boys");
    printWithoutPartner(girlHeightLeftovers, "girls");
  }

  private static void printPairs(int[] boyHeights, int[] girlHeights) {
    StringBuilder pairs = new StringBuilder();
    for (int i = 0; i < boyHeights.length; i++) {
      pairs.append("(").append(boyHeights[i]).append(", ").append(girlHeights[i]).append(") ");
    }
    System.out.println("Dance pairs are: " + pairs.toString().trim());
  }

  private static void printWithoutPartner(int[] leftovers, String gender) {
    if (leftovers.length > 0) {
      System.out.println("The following " + gender + " were left without a partner: " + Arrays.toString(leftovers));
    }
  }

}
