import java.util.Arrays;
import java.util.stream.IntStream;

public class ArrayJoiningAndReversing {

  private static final int[] SOURCE_ARRAY_1 = {-10, 17, 21};
  private static final int[] SOURCE_ARRAY_2 = {-2, 5, 17, 28};

  public static void main(String[] args) {
    System.out.println("Combined array is: " + Arrays.toString(combineSourceArrays()));
    System.out.println("Reversed SOURCE_ARRAY_2 is " + Arrays.toString(reverseSourceArray2()));
  }

  // https://stackoverflow.com/questions/4697255/combine-two-integer-arrays
  private static int[] combineSourceArrays() {
    int[] combinedArray = new int[SOURCE_ARRAY_1.length + SOURCE_ARRAY_2.length];
    System.arraycopy(SOURCE_ARRAY_1, 0, combinedArray, 0, SOURCE_ARRAY_1.length);
    System.arraycopy(SOURCE_ARRAY_2, 0, combinedArray, SOURCE_ARRAY_1.length, SOURCE_ARRAY_2.length);
    Arrays.sort(combinedArray);
    return combinedArray;
  }

  // https://stackoverflow.com/questions/2137755/how-do-i-reverse-an-int-array-in-java
  private static int[] reverseSourceArray2() {
    return IntStream.rangeClosed(1, SOURCE_ARRAY_2.length).map(i -> -SOURCE_ARRAY_2[SOURCE_ARRAY_2.length - i]).toArray();
  }

}
