public class Rippsild {

    private char sillaTähis;
    private int läbimiseAeg;

    public Rippsild(char sillaTähis, int läbimiseAeg) {
        this.läbimiseAeg = läbimiseAeg;
        this.sillaTähis = sillaTähis;
    }

    // kui lisada meetodi signatuuri keyword synchronized, siis
    // korraga saab ainult 1 thread seda meetodit samal ajal access'ida.
    // teine thread ootab, kuni esimene thread on meetodist väljunud
    // NB! Rippsildu on 2 tükki, seega sünkroniseerimislukke on samuti 2,
    // mis tähendab, et igat silda saab korraga ületada 1 inimene (synchronized) või lõpmetu arv inimesi (non synchronzied)
    public void ületa(Seikleja seikleja) throws InterruptedException {
        System.out.println(seikleja.getNimi() + " astus sillale " + sillaTähis + " "
                + ((System.currentTimeMillis() - seikleja.getAlgusAeg()) / 1000));
        Thread.sleep(1000 * läbimiseAeg);
        System.out.println(seikleja.getNimi() + " lahkus sillalt " + sillaTähis + " "
                + (System.currentTimeMillis() - seikleja.getAlgusAeg()) / 1000);
    }

}