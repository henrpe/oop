class DancePairApplication {

    public static void main(String[] args) {
        DancePair ericAndDonna = new DancePair("Eric Forman", 1970, "Donna Pinciotti", 1971);
        DancePair michaelAndJackie = new DancePair("Michael Kelso", 1972, "Jackie Burkhart", 1972);

        System.out.println(ericAndDonna.getOlderPartnerAge(2021));
        System.out.println(ericAndDonna.isPartnersBirthYearTheSame());
        System.out.println(ericAndDonna);

        System.out.println(michaelAndJackie.getOlderPartnerAge(2021));
        System.out.println(michaelAndJackie.isPartnersBirthYearTheSame());
        System.out.println(michaelAndJackie);
    }

}
