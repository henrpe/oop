class IdCard {

  private final String number;
  private final String name;
  private final String code;

  IdCard(String number, String name, String code) {
    this.number = number;
    this.name = name;
    this.code = code;
  }

  @Override
  public String toString() {
    return "IdCard{" +
        "number='" + number + '\'' +
        ", name='" + name + '\'' +
        ", code='" + code + '\'' +
        ", gender='" + getGender() + '\'' +
        ", birthDate='" + getBirthDate() + '\'' +
        '}';
  }

  private String getGender() {
    switch (getGenderCode()) {
      case '1':
      case '3':
      case '5':
        return "male";
      case '2':
      case '4':
      case '6':
        return "female";
      default:
        throw new RuntimeException("personal code format error: unknown gender");
    }
  }

  private String getBirthDate() {
    String day = code.substring(5, 7);
    String month = code.substring(3, 5);
    return day + '.' + month + '.' + getYear();
  }

  private String getYear() {
    String year = code.substring(1, 3);
    switch (getGenderCode()) {
      case '1':
      case '2':
        return "18" + year;
      case '3':
      case '4':
        return "19" + year;
      case '5':
      case '6':
        return "20" + year;
      default:
        throw new RuntimeException("personal code format error:: year cannot be determined");
    }
  }

  private char getGenderCode() {
    return code.charAt(0);
  }

}

