public class IdCardApplication {

  public static void main(String[] args) {
    createIdCardAndPrint("1", "Toomas Tamm", "38903040001");
    createIdCardAndPrint("2", "Mari Tamm", "49105210001");
    createIdCardAndPrint("3", "Johannes Tamm", "19807230001");
    createIdCardAndPrint("4", "Miina Tamm", "29902110001");
    createIdCardAndPrint("5", "Mati Tamm", "50204170001");
    createIdCardAndPrint("6", "Anu Tamm", "60105160001");
  }

  private static void createIdCardAndPrint(String number, String name, String code) {
    IdCard idCard = new IdCard(number, name, code);
    System.out.println(idCard.toString());
  }

}
