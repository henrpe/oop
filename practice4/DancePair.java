class DancePair {

    private String boyName;
    private int boyBirthYear;
    private String girlName;
    private int girlBirthYear;

    DancePair(String boyName, int boyBirthYear, String girlName, int girlBirthYear) {
        this.boyName = boyName;
        this.boyBirthYear = boyBirthYear;
        this.girlName = girlName;
        this.girlBirthYear = girlBirthYear;
    }

    String getBoyName() {
        return boyName;
    }

    void setBoyName(String boyName) {
        this.boyName = boyName;
    }

    int getBoyBirthYear() {
        return boyBirthYear;
    }

    void setBoyBirthYear(int boyBirthYear) {
        this.boyBirthYear = boyBirthYear;
    }

    String getGirlName() {
        return girlName;
    }

    void setGirlName(String girlName) {
        this.girlName = girlName;
    }

    int getGirlBirthYear() {
        return girlBirthYear;
    }

    void setGirlBirthYear(int girlBirthYear) {
        this.girlBirthYear = girlBirthYear;
    }

    int getOlderPartnerAge(int currentYear) {
        return currentYear - Math.min(this.boyBirthYear, this.girlBirthYear);
    }

    boolean isPartnersBirthYearTheSame() {
        return this.boyBirthYear == this.girlBirthYear;
    }

    String getPartnersLastNames() {
        return getLastName(this.boyName) + "-" + getLastName(this.girlName);
    }

    // https://stackoverflow.com/questions/1181969/java-get-last-element-after-split/1181976
    private String getLastName(String fullName) {
        return fullName.substring(fullName.lastIndexOf(" ") + 1);
    }

    @Override
    public String toString() {
        return "Dancepair is: " + this.getPartnersLastNames();
    }

}
