import java.time.LocalDateTime;

class VäliseMonitorigaArvuti extends Arvuti {

    VäliseMonitorigaArvuti(String tootja, boolean onKiirtöö, LocalDateTime registreerimiseAeg) {
        super(tootja, onKiirtöö, registreerimiseAeg);
    }

    @Override
    double arvutaArveSumma(double alghind) {
        return super.arvutaArveSumma(alghind) + 1;
    }

    @Override
    public String toString() {
        return super.toString() + " ja mul on väline monitor";
    }
}
