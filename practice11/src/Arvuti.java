import java.time.LocalDateTime;

class Arvuti {

    private final String tootja;
    private final boolean kiirtöö;
    private final LocalDateTime registreerimiseAeg;
    private double arveSumma;

    public Arvuti(String tootja, boolean kiirtöö, LocalDateTime registreerimiseAeg) {
        this.tootja = tootja;
        this.kiirtöö = kiirtöö;
        this.registreerimiseAeg = registreerimiseAeg;
    }

    String getTootja() {
        return tootja;
    }

    boolean isKiirtöö() {
        return kiirtöö;
    }

    double getArveSumma() {
        return arveSumma;
    }

    void setArveSumma(double arveSumma) {
        this.arveSumma = arveSumma;
    }

    LocalDateTime getRegistreerimiseAeg() {
        return registreerimiseAeg;
    }

    double arvutaArveSumma(double alghind) {
        double hind = alghind + 2;
        if (this.isKiirtöö()) {
            hind = hind + 10;
        }
        return hind;
    }

    @Override
    public String toString() {
        return "Arvuti{" +
                "tootja='" + tootja + '\'' +
                ", kiirtöö=" + kiirtöö +
                ", registreerimiseAeg=" + registreerimiseAeg +
                '}';
    }
}
