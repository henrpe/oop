import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;

class Arvutiparandus {

    public static void main(String[] args) throws IOException {
        String töödenimekiri = args[0];

        InputStream inputStream = töödenimekiri.startsWith("http://") || töödenimekiri.startsWith("https://") ?
                new URL(töödenimekiri).openStream() : new FileInputStream(töödenimekiri);

        List<Arvuti> parandamistVajavadArvutid = getParandamistVajavadArvutid(inputStream);
        List<Arvuti> parandatudArvutid = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Kas soovid parandada (P), uut tööd registreerida (R) või lõpetada (L) ?");
                String input = bufferedReader.readLine();
                if ("P".equals(input)) {
                    parandaArvuti(parandamistVajavadArvutid, parandatudArvutid, bufferedReader);
                } else if ("R".equals(input)) {
                    registreeriUusTöö(parandamistVajavadArvutid, bufferedReader);
                } else if ("L".equals(input)) {
                    salvestaTehtudTööd(parandatudArvutid);
                    salvestaTegemataTööd(parandamistVajavadArvutid);
                    väljastaÜlevaade(parandatudArvutid, parandamistVajavadArvutid.size());
                    return;
                }
            }
        }

    }

    private static void parandaArvuti(List<Arvuti> parandamistVajavadArvutid, List<Arvuti> parandatudArvutid, BufferedReader bufferedReader) throws IOException {
        if (parandamistVajavadArvutid.size() > 0) {
            Arvuti parandatavArvuti = null;
            for (Arvuti arvuti : parandamistVajavadArvutid) {
                if (arvuti.isKiirtöö()) {
                    parandatavArvuti = arvuti;
                    break;
                }
            }
            if (parandatavArvuti == null) {
                parandatavArvuti = parandamistVajavadArvutid.get(0);
            }

            System.out.println("Arvuti info: " + parandatavArvuti);

            Map<String, Double> tunnitasudeMap = new HashMap<>();
            try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("practice11/tunnitasud.dat"))) {
                int töötajateArv = dataInputStream.readInt();
                for (int i = 0; i < töötajateArv; i++) {
                    String nimi = dataInputStream.readUTF();
                    Double tunnitasu = dataInputStream.readDouble();
                    tunnitasudeMap.put(nimi, tunnitasu);
                }
            }

            while (true) {
                double aegMinutites;
                try {
                    System.out.println("Sisesta parandamiseks kulunud aeg (täisminutites):");
                    aegMinutites = Double.parseDouble(bufferedReader.readLine());
                } catch (Exception e) {
                    continue;
                }
                System.out.println("Sisesta nimi:");
                String nimi = bufferedReader.readLine();
                if (tunnitasudeMap.containsKey(nimi)) {
                    double baashind = aegMinutites / 60 * tunnitasudeMap.get(nimi);
                    double arveSumma = parandatavArvuti.arvutaArveSumma(baashind);

                    // https://java2blog.com/format-double-to-2-decimal-places-java/
                    BigDecimal arveSummaBigDecimal = new BigDecimal(arveSumma).setScale(2, RoundingMode.HALF_UP);
                    parandatavArvuti.setArveSumma(arveSummaBigDecimal.doubleValue());
                    System.out.println("Töö tehtud, arve summa on: " + arveSummaBigDecimal.doubleValue() + "\n");
                    break;
                }
                System.out.println("Sellise nimega töötaja puudub!");
            }

            parandamistVajavadArvutid.remove(parandatavArvuti);
            parandatudArvutid.add(parandatavArvuti);
        }
    }

    private static List<Arvuti> getParandamistVajavadArvutid(InputStream inputStream) throws IOException {
        List<Arvuti> parandamistVajavadArvutid = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String tööKirjeldus = bufferedReader.readLine();
            while (tööKirjeldus != null) {
                try {
                    parandamistVajavadArvutid.add(loeArvuti(tööKirjeldus));
                } catch (FormaadiErind formaadiErind) {
                    System.out.println("Rida: " + tööKirjeldus + " Vea selgitus: " + formaadiErind.getMessage());
                }
                tööKirjeldus = bufferedReader.readLine();
            }
        }
        return parandamistVajavadArvutid;
    }

    private static void registreeriUusTöö(List<Arvuti> parandamistVajavadArvutid, BufferedReader bufferedReader) throws IOException {
        while (true) {
            System.out.println("Sisesta töö kirjeldus: tootja;tavatöö/kiirtöö;monitoriga?");
            try {
                String tööKirjeldus = bufferedReader.readLine();
                parandamistVajavadArvutid.add(loeArvuti(tööKirjeldus));
                System.out.println("Töö on registreeritud!\n");
                break;
            } catch (FormaadiErind formaadiErind) {
                System.out.println(formaadiErind.getMessage());
            }
        }
    }

    private static void salvestaTehtudTööd(List<Arvuti> arvutid) throws IOException {
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("practice11/tehtud.dat"))) {
            dataOutputStream.writeInt(arvutid.size());
            for (Arvuti arvuti : arvutid) {
                dataOutputStream.writeUTF(arvuti.getTootja());
                dataOutputStream.writeUTF(arvuti.getRegistreerimiseAeg().toString());
                dataOutputStream.writeDouble(arvuti.getArveSumma());
            }
        }
    }

    private static void salvestaTegemataTööd(List<Arvuti> arvutid) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream("practice11/ootel.txt")) {
            for (Arvuti arvuti : arvutid) {
                String tootja = arvuti.getTootja();
                String töö = arvuti.isKiirtöö() ? ";kiirtöö" : ";tavatöö";
                String monitoriga = arvuti instanceof VäliseMonitorigaArvuti ? ";monitoriga" : "";
                String registreerimisAeg = "@" + arvuti.getRegistreerimiseAeg().toString();
                fileOutputStream.write((tootja + töö + monitoriga + registreerimisAeg + "\n").getBytes());
            }
        }
    }

    private static Arvuti loeArvuti(String tööKirjeldus) {
        int at = tööKirjeldus.lastIndexOf('@');
        LocalDateTime registreerimiseAeg;
        if (at != -1) {
            registreerimiseAeg = LocalDateTime.parse(tööKirjeldus.substring(at + 1));
            tööKirjeldus = tööKirjeldus.substring(0, at);
        } else {
            registreerimiseAeg = LocalDateTime.now();
        }

        String[] tööKirjelduseOsad = tööKirjeldus.split(";");
        if (tööKirjelduseOsad.length < 2 || tööKirjelduseOsad.length > 3) {
            throw new FormaadiErind("Semikooloniga eraldatud väljade arv on väiksem kui kaks või suurem kui kolm");
        }
        if (!Arrays.asList("tavatöö", "kiirtöö").contains(tööKirjelduseOsad[1])) {
            throw new FormaadiErind("öö tüübi väljas olev väärtus ei ole \"tavatöö\" ega \"kiirtöö\"");
        }
        if (tööKirjelduseOsad.length == 3 && !"monitoriga".equals(tööKirjelduseOsad[2])) {
            throw new FormaadiErind("väljade arv on kolm, aga kolmanda välja väärtus ei ole \"monitoriga\"");
        }

        String tootja = tööKirjelduseOsad[0];
        boolean onKiirtöö = "kiirtöö".equals(tööKirjelduseOsad[1]);
        if (tööKirjelduseOsad.length == 3) {
            return new VäliseMonitorigaArvuti(tootja, onKiirtöö, registreerimiseAeg);
        }
        return new Arvuti(tootja, onKiirtöö, registreerimiseAeg);
    }

    private static void väljastaÜlevaade(List<Arvuti> parandatudArvutid, int ootel) {
        System.out.println("Sessiooni kokkuvõte:");

        // https://www.baeldung.com/java-stream-sum
        double summa = parandatudArvutid.stream().mapToDouble(Arvuti::getArveSumma).sum();
        System.out.println("Teenitud raha: " + Math.round(summa * 100.0) / 100.0 + "€");

        System.out.println("Parandatud arvuteid: ");

        Map<String, Integer> erinevadArvutid = new HashMap<>();

        for (Arvuti arvuti : parandatudArvutid) {
            String tootja = arvuti.getTootja();
            if (erinevadArvutid.containsKey(tootja)) {
                int arvutiteArv = erinevadArvutid.get(tootja) + 1;
                erinevadArvutid.put(tootja, arvutiteArv);
            } else
                erinevadArvutid.put(tootja, 1);
        }

        for (String tootja : erinevadArvutid.keySet())
            System.out.println("  " + tootja + ": " + erinevadArvutid.get(tootja) + "tk");

        System.out.println("Ootele jäi " + ootel + " arvuti(t).");
    }

}
