package oop;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MousePosition extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  private static final Canvas canvas = new Canvas(400, 400);

  @Override
  public void start(Stage stage) {
    Pane pane = new Pane(canvas);
    Scene scene = new Scene(pane);
    stage.setScene(scene);
    stage.setTitle("Mouse location when mouse button is pressed");
    stage.setResizable(false);

    canvas.setOnMousePressed(event -> drawMouseLocation(event));
    canvas.setOnMouseDragged(event -> drawMouseLocation(event));
    canvas.setOnMouseReleased(event -> removeMouseLocation());

    stage.show();
  }

  private void drawMouseLocation(MouseEvent mouseEvent) {
    double mouseEventX = mouseEvent.getX();
    double mouseEventY = mouseEvent.getY();
    String mouseLocation = "(" + mouseEventX + ", " + mouseEventY + ")";
    removeMouseLocation();
    GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
    graphicsContext.setFill(Color.BLACK);
    graphicsContext.fillText(mouseLocation, mouseEventX, mouseEventY);
  }

  private void removeMouseLocation() {
    canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
  }

}