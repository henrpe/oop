import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApacheCommonsTest {
    @Test
    public void isAllUpperCaseShouldWorkAsDocumented() {
        assertTrue(StringUtils.isAllUpperCase("ASDASD"));

        assertFalse(StringUtils.isAllUpperCase(null));
        assertFalse(StringUtils.isAllUpperCase(""));
        assertFalse(StringUtils.isAllUpperCase("  "));
        assertFalse(StringUtils.isAllUpperCase("aBC"));
        assertFalse(StringUtils.isAllUpperCase("A C"));
        assertFalse(StringUtils.isAllUpperCase("A1C"));
        assertFalse(StringUtils.isAllUpperCase("A/C"));
    }
}
