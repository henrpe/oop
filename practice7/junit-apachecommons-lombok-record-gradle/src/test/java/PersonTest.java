import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PersonTest {
    @Test
    public void personRecord() {
        var person = new PersonRecord("John Smith", 55, true);
        assertEquals(person.name(), "John Smith");
        assertEquals(person.age(), 55);
        assertTrue(person.alive());
    }

    @Test
    public void personLombok() {
        var person = new PersonLombok("John Smith", 55, true);
        assertEquals(person.getName(), "John Smith");
        assertEquals(person.getAge(), 55);
        assertTrue(person.isAlive());
    }
}
