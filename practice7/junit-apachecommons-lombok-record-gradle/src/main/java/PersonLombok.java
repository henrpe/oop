import lombok.AllArgsConstructor;
import lombok.Data;

@Data // https://projectlombok.org/features/Data
@AllArgsConstructor // https://projectlombok.org/features/constructor
public class PersonLombok {
    private String name;
    private int age;
    private boolean alive;
}
