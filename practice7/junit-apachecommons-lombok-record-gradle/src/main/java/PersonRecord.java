public record PersonRecord(String name, int age, boolean alive) {}
