# Running

In project root folder: `./gradlew run` (mac/linux) or `gradlew run` (windows)

# Credit

Used code samples from: https://gist.github.com/jewelsea/5094893 and https://mkyong.com/java/java-read-a-file-from-resources-folder/