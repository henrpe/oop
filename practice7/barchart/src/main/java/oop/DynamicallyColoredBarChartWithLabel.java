package oop;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays a bar with a single series whose bars are different colors depending upon the bar value.
 * A custom legend is created and displayed for the bar data.
 * Bars in the chart are customized to include a text label of the bar's data value above the bar.
 */
public class DynamicallyColoredBarChartWithLabel extends Application {
  @Override
  public void start(Stage stage) {
    final CategoryAxis xAxis = new CategoryAxis();
    xAxis.setLabel("Bars");
    final NumberAxis yAxis = new NumberAxis();
    yAxis.setLabel("Value");

    final BarChart<String, Number> bc = new BarChart<>(xAxis, yAxis);
    bc.setLegendVisible(false);

    XYChart.Series series1 = new XYChart.Series();

    List<Integer> dataFromDisk = readDataFromDisk();
    for (int i = 0; i < dataFromDisk.size(); i++) {
      Integer value = dataFromDisk.get(i);
      final XYChart.Data<String, Number> data = new XYChart.Data(String.valueOf(value), value);
      data.nodeProperty().addListener(new ChangeListener<Node>() {
        @Override
        public void changed(ObservableValue<? extends Node> ov, Node oldNode, final Node node) {
          if (node != null) {
            setNodeStyle(data);
          }
        }
      });
      series1.getData().add(data);
    }
    bc.getData().add(series1);

    LevelLegend legend = new LevelLegend();
    legend.setAlignment(Pos.CENTER);

    VBox chartWithLegend = new VBox();
    chartWithLegend.getChildren().setAll(bc, legend);
    VBox.setVgrow(bc, Priority.ALWAYS);

    chartWithLegend.getStylesheets().add(getClass().getResource("colored-chart.css").toExternalForm());

    stage.setScene(new Scene(chartWithLegend));
    stage.setMinHeight(400);
    stage.setMinWidth(400);
    stage.show();
  }

  /**
   * Change color of bar: if value is > 50 then red, else blue
   */
  private void setNodeStyle(XYChart.Data<String, Number> data) {
    Node node = data.getNode();
    if (data.getYValue().intValue() > 50) {
      node.setStyle("-fx-bar-fill: -fx-red;");
    } else {
      node.setStyle("-fx-bar-fill: -fx-blue;");
    }
  }

  private List<Integer> readDataFromDisk() {
    ArrayList<Integer> result = new ArrayList<>();

    try (InputStream inputStream = getClass().getResourceAsStream("/data.txt");
         BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
      String line;
      while ((line = br.readLine()) != null) {
        result.add(Integer.parseInt(line));
      }
    } catch (IOException e) {
      throw new RuntimeException("File reading failed", e);
    }
    return result;
  }

  public static void main(String[] args) {
    launch(args);
  }
}