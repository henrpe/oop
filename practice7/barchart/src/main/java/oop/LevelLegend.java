package oop;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;

/** A simple custom legend for a two valued chart. */
class LevelLegend extends GridPane {
  LevelLegend() {
    setHgap(10);
    setVgap(10);
    addRow(0, createSymbol("-fx-red"),     new Label("Red"));
    addRow(1, createSymbol("-fx-blue"),     new Label("Blue"));

    getStyleClass().add("level-legend");
  }

  /** Create a custom symbol for a custom chart legend with the given fillStyle style string. */
  private Node createSymbol(String fillStyle) {
    Shape symbol = new Ellipse(10, 5, 10, 5);
    symbol.setStyle("-fx-fill: " + fillStyle);
    symbol.setStroke(Color.BLACK);
    symbol.setStrokeWidth(2);

    return symbol;
  }
}