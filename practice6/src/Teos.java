abstract class Teos implements Comparable<Teos> {

    // Access levels: https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html
    // Piisab no modifier ehk package-local / package-private nähtavusest
    String teoseKirjeldus;
    String teoseTähis;
    String laenutajaNimi;
    int päevadeArv;

    Teos(String teoseKirjeldus, String teoseTähis, String laenutajaNimi, int päevadeArv) {
        this.teoseKirjeldus = teoseKirjeldus;
        this.teoseTähis = teoseTähis;
        this.laenutajaNimi = laenutajaNimi;
        this.päevadeArv = päevadeArv;
    }

    abstract boolean kasHoidlast();

    void arvutaViivis(Kontrollija kontrollija) {
        int üleajaPäevad = päevadeArv - laenutusaeg();
        if (üleajaPäevad > 0) {
            double viiviseSuurus = päevaneViivis() * üleajaPäevad;
            kontrollija.salvestaViivis(laenutajaNimi, teoseKirjeldus, viiviseSuurus);
        }
    }

    private int laenutusaeg() {
        switch (teoseTähis) {
            case "roheline":
                return 1;
            case "kollane":
                return 30;
            case "sinine":
                return 60;
            case "puudub":
                return 14;
            default:
                return 0;
        }
    }

    private double päevaneViivis() {
        switch (teoseTähis) {
            case "roheline":
                return 2;
            case "kollane":
            case "sinine":
                return 0.05;
            case "puudub":
                return 0.15;
            default:
                return 0;
        }
    }

    @Override
    public String toString() {
        return "Teos{" +
            "teoseKirjeldus='" + teoseKirjeldus + '\'' +
            ", teoseTähis='" + teoseTähis + '\'' +
            ", laenutajaNimi='" + laenutajaNimi + '\'' +
            ", päevadeArv=" + päevadeArv +
            ", kasHoidlast=" + kasHoidlast() +
            '}';
    }

    @Override
    public int compareTo(Teos teos) {
        return this.teoseKirjeldus.compareTo(teos.teoseKirjeldus);
    }

}
