class SuurimaViiviseLeidja implements Kontrollija {

    private String suurimaViivisegaLaenutaja;
    private String suurimaViivisegaLaenutajaTeoseKirjeldus;
    private double suurimaViivisegaLaenutajaViiviseSuurus;

    // Ei arvesta olukorda, kus sama inimene võib mitut raamatut laenutada ning võlgu olla nende eest
    @Override
    public void salvestaViivis(String laenutajaNimi, String teoseKirjeldus, double viiviseSuurus) {
        if (viiviseSuurus > suurimaViivisegaLaenutajaViiviseSuurus) {
            suurimaViivisegaLaenutaja = laenutajaNimi;
            suurimaViivisegaLaenutajaViiviseSuurus = viiviseSuurus;
            suurimaViivisegaLaenutajaTeoseKirjeldus = teoseKirjeldus;
        }
    }

    void saadaHoiatus() {
        System.out.println(
            "Suurima viivisega laenutaja on " + suurimaViivisegaLaenutaja +
                " ning tema teose kirjeldus on " + suurimaViivisegaLaenutajaTeoseKirjeldus
        );
    }

}
