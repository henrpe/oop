import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

class Peaklass {

    // This project has a directory in between the running directory and file(s)
    // If you don't have this directory in between then it should be removed from the following constant(s)
    // To find out the running directory: System.out.println(System.getProperty("user.dir"));
    private static final String INPUT_FILE = "practice6/laenutus.txt";

    public static void main(String[] args) throws Exception {
        List<Teos> teosed = loeTeosed(INPUT_FILE);
        sort(teosed);
        teosed.forEach(teos -> System.out.println(teos));

        ViiviseHoiataja viiviseHoiataja = new ViiviseHoiataja(0.2);
        teosed.forEach(teos -> teos.arvutaViivis(viiviseHoiataja));
        System.out.println(viiviseHoiataja.getHoiatatavadLaenutajad());

        SuurimaViiviseLeidja suurimaViiviseLeidja = new SuurimaViiviseLeidja();
        teosed.forEach(teos -> teos.arvutaViivis(suurimaViiviseLeidja));
        suurimaViiviseLeidja.saadaHoiatus();
    }

    private static List<Teos> loeTeosed(String failinimi) throws Exception {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(failinimi), UTF_8))) {
            return bufferedReader.lines().map(rida -> rida.split("; ")).map(reaOsad -> reaOsad[0].contains("/") ?
                new Ajakiri(reaOsad[0], reaOsad[1], reaOsad[2], Integer.parseInt(reaOsad[3])) :
                new Raamat(reaOsad[0], reaOsad[1], reaOsad[2], Integer.parseInt(reaOsad[3]))).collect(toList());
        }
    }

}
