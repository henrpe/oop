class Ajakiri extends Teos {

    Ajakiri(String teoseKirjeldus, String teoseTähis, String laenutajaNimi, int päevadeArv) {
        super(teoseKirjeldus, teoseTähis, laenutajaNimi, päevadeArv);
    }

    // https://stackoverflow.com/questions/12595019/how-to-get-a-string-between-two-characters
    @Override
    boolean kasHoidlast() {
        int aasta = Integer.parseInt(teoseKirjeldus.substring(teoseKirjeldus.indexOf("/") + 1, teoseKirjeldus.indexOf(",")));
        return aasta <= 2000;
    }

    @Override
    public String toString() {
        return super.toString() + " on ajakiri.";
    }

}
