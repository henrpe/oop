import java.io.*;
import java.util.*;

class Vaktsineerimiskeskus {

    public static void main(String[] args) throws IOException {
        // This project has a directory in between the running directory and file(s)
        // If you don't have this directory in between then it should be removed from the following constant(s)
        // To find out the running directory: System.out.println(System.getProperty("user.dir"));
        Queue<Broneering> broneeringud = loeBroneeringud("KT2_2021/broneeringud.dat");
        List<Broneering> lõpetatudBroneeringud = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Palun valige üks järgmistest tegevustest:");
                System.out.println("(J)ärgmine broneering, (V)aata lõpetatud broneeringuid vaktsiinide kaupa, (S)alvesta ja lõpeta");
                String input = bufferedReader.readLine();
                if ("J".equals(input)) {
                    järgmineBroneering(broneeringud, lõpetatudBroneeringud);
                } else if ("V".equals(input)) {
                    vaataLõpetatudBroneeringuid(bufferedReader, lõpetatudBroneeringud);
                } else if ("S".equals(input)) {
                    salvestaLõpetatudBroneeringud(lõpetatudBroneeringud);
                    return;
                }
            }
        }
    }

    private static void järgmineBroneering(Queue<Broneering> broneeringud, List<Broneering> lõpetatudBroneeringud) {
        Broneering järgmineBroneering = broneeringud.poll();
        if (järgmineBroneering == null) {
            System.out.println("Ootel broneeringuid pole!");
        } else {
            System.out.println(järgmineBroneering);
            lõpetatudBroneeringud.add(järgmineBroneering);
        }
    }

    private static void vaataLõpetatudBroneeringuid(BufferedReader bufferedReader, List<Broneering> lõpetatudBroneeringud) throws IOException {
        if (lõpetatudBroneeringud.size() == 0) {
            System.out.println("Lõpetatud broneeringud puuduvad");
            return;
        }
        Map<String, List<Broneering>> vaktsiinideKaupa = vaktsiinideKaupa(lõpetatudBroneeringud);
        System.out.println("Vaktsiinide nimetused: " + vaktsiinideKaupa.keySet());
        while (true) {
            System.out.println("Millise vaktsiini kohta soovid infot näha?");
            String input = bufferedReader.readLine();
            if (vaktsiinideKaupa.containsKey(input)) {
                System.out.println("Lõpetatud broneeringud " + input + " kohta: " + vaktsiinideKaupa.get(input));
                return;
            }
        }
    }

    private static void salvestaLõpetatudBroneeringud(List<Broneering> lõpetatudBroneeringud) throws IOException {
        Map<String, List<Broneering>> salvestatavateBroneeringuteMap = vaktsiinideKaupa(lõpetatudBroneeringud);
        for (String vaktsiin : salvestatavateBroneeringuteMap.keySet()) {
            // This project has a directory in between the running directory and file(s)
            // If you don't have this directory in between then it should be removed from the following constant(s)
            // To find out the running directory: System.out.println(System.getProperty("user.dir"));
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("KT2_2021/" + vaktsiin.toLowerCase() + ".txt"))) {
                List<Broneering> salvestatavadBroneeringud = salvestatavateBroneeringuteMap.get(vaktsiin);
                for (Broneering salvestatavBroneering : salvestatavadBroneeringud) {
                    bufferedWriter.write(
                            salvestatavBroneering.getNimi() + ";" +
                                    salvestatavBroneering.getAeg() + ";" +
                                    salvestatavBroneering.getMitmes() + "\n");
                }
            }
        }
    }

    private static Queue<Broneering> loeBroneeringud(String failiNimi) throws IOException {
        Queue<Broneering> broneeringud = new LinkedList<>();
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(failiNimi))) {
            int broneeringuteArv = dataInputStream.readInt();
            for (int i = 0; i < broneeringuteArv; i++) {
                String nimi = dataInputStream.readUTF();
                String aeg = dataInputStream.readUTF();
                String vaktsiin = dataInputStream.readUTF();
                int mitmes = dataInputStream.readInt();
                try {
                    broneeringud.add(new Broneering(nimi, aeg, vaktsiin, mitmes));
                } catch (EbasobivArvErind ebasobivArvErind) {
                    System.out.println(ebasobivArvErind.getMessage());
                }
            }
        }
        return broneeringud;
    }

    private static Map<String, List<Broneering>> vaktsiinideKaupa(List<Broneering> lõpetatudBroneeringud) {
        Map<String, List<Broneering>> broneeringuteMap = new HashMap<>();
        for (Broneering broneering : lõpetatudBroneeringud) {
            if (broneeringuteMap.containsKey(broneering.getVaktsiin())) {
                broneeringuteMap.get(broneering.getVaktsiin()).add(broneering);
            } else {
                List<Broneering> broneeringud = new ArrayList<>();
                broneeringud.add(broneering);
                broneeringuteMap.put(broneering.getVaktsiin(), broneeringud);
            }
        }
        return broneeringuteMap;
    }

}
