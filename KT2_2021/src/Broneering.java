class Broneering {

    private final String nimi;
    private final String aeg;
    private final String vaktsiin;
    private final int mitmes;

    Broneering(String nimi, String aeg, String vaktsiin, int mitmes) {
        if (mitmes != 1 && mitmes != 2) {
            throw new EbasobivArvErind(
                    String.format("Ebasobiv broneering: %s, kell %s, vaktsiin %s, %d. kord", nimi, aeg, vaktsiin, mitmes)
            );
        }
        this.nimi = nimi;
        this.aeg = aeg;
        this.vaktsiin = vaktsiin;
        this.mitmes = mitmes;
    }

    String getNimi() {
        return nimi;
    }

    String getAeg() {
        return aeg;
    }

    String getVaktsiin() {
        return vaktsiin;
    }

    int getMitmes() {
        return mitmes;
    }

    @Override
    public String toString() {
        return "Broneering{" +
                "nimi='" + nimi + '\'' +
                ", aeg='" + aeg + '\'' +
                ", vaktsiin='" + vaktsiin + '\'' +
                ", mitmes=" + mitmes +
                '}';
    }

}
