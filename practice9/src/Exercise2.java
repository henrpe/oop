import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Exercise2 {

  // https://stackoverflow.com/questions/18139493/finding-first-word-after-regex-match
  public static void main(String[] args) throws IOException {
    BufferedReader bufferedReader = Files.newBufferedReader(Paths.get("hidden.txt"), Charset.defaultCharset());
    BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get("passwords.txt"), Charset.defaultCharset(), StandardOpenOption.CREATE);
    Pattern pattern = Pattern.compile("(?<=parool: )(\\w+)");
    String line;
    while ((line = bufferedReader.readLine()) != null) {
      Matcher matcher = pattern.matcher(line);
      while (matcher.find()) {
        bufferedWriter.write(matcher.group() + "\n");
      }
    }
    bufferedReader.close();
    bufferedWriter.close();
  }

}
