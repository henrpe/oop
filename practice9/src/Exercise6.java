import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

class Exercise6 {

  // https://stackoverflow.com/questions/49150047/java-file-read-and-write-in-reverse-byte-by-byte
  public static void main(String[] args) throws IOException {
    RandomAccessFile randomAccessFile = new RandomAccessFile("source.txt", "r");
    FileOutputStream fileOutputStream = new FileOutputStream("destination.txt");
    for (long byteLocation = randomAccessFile.length() - 1; byteLocation >= 0; byteLocation--) {
      randomAccessFile.seek(byteLocation);
      int sourceByte = randomAccessFile.read();
      fileOutputStream.write(sourceByte);
    }
    randomAccessFile.close();
    fileOutputStream.close();
  }

}
