import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Exercise5 {
    private static final String DAT_FILE = "person.dat";

    public static void main(String[] args) throws IOException {
        Person personToFile = new Person("John", "Smith", List.of("Mary", "Jane"));
        writePerson(personToFile);
        Person personFromFile = readPerson();
        System.out.println(personFromFile);
    }

    private static void writePerson(Person person) throws IOException {
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(DAT_FILE))) {
            dataOutputStream.writeUTF(person.firstName());
            dataOutputStream.writeUTF(person.lastName());
            dataOutputStream.writeInt(person.childrenNames().size());
            for (String childName : person.childrenNames()) {
                dataOutputStream.writeUTF(childName);
            }
        }
    }

    private static Person readPerson() throws IOException {
        Person person;
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(DAT_FILE))) {
            String firstName = dataInputStream.readUTF();
            String lastName = dataInputStream.readUTF();
            List<String> childrenNames = new ArrayList<>();
            int childrenCount = dataInputStream.readInt();
            for (int i = 0; i < childrenCount; i++) {
                childrenNames.add(dataInputStream.readUTF());
            }
            person = new Person(firstName, lastName, childrenNames);
        }
        return person;
    }
}

record Person(String firstName, String lastName, List<String> childrenNames) {
}