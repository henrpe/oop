import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Collectors;

class Exercise1And3 {

  // https://www.baeldung.com/reading-file-in-java
  // https://www.baeldung.com/java-write-to-file
  // https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/nio/file/Files.html
  public static void main(String[] args) throws IOException {
    System.out.println(Charset.defaultCharset());

    String inputData = "Andje käsi väsis ärä, võtje käsi ei väsi kunagi";
    String fileName = "data.txt";

    BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get(fileName), Charset.defaultCharset(), StandardOpenOption.CREATE);
    bufferedWriter.write(inputData);
    bufferedWriter.close();

    BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(fileName), Charset.defaultCharset());
    String outputData = bufferedReader.lines().collect(Collectors.joining(System.lineSeparator()));
    bufferedReader.close();
    System.out.println(outputData);
  }

}
